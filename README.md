#brainfuck2
######this will make it easier to make your [brainfuck](//en.wikipedia.org/wiki/Brainfuck)ed

###Subprojects
* [libbrainfuck2](libbrainfuck2/README.md)

###Features
* none yet

###Roadmap
* library to make brainfuck portable and embeddable
* add more language features
    * functions
    * bare strings
    * RLE encoded commands
* memory dump
* input and output stream

###Acknowledgement

Brainfuck2 is based on the esoteric programming language Brainfuck created by Urban Müller  