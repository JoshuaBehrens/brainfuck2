#ifndef __LIBBRAINFUCK2_ENGINE_H
#define __LIBBRAINFUCK2_ENGINE_H

#ifdef __cplusplus

#include "Engine.hpp"

using brainfuck2::Engine;

extern "C"
{
#else

struct Engine;

#endif

/**
 * Creates a new Engine instance
 * @author Joshua Behrens
 * @return Engine* A pointer to a new Engine instance
 */
Engine* bf2_engine_create(void);

/**
 * Destroys the given Engine instance and clears the pointer
 * @author Joshua Behrens
 * @param Engine** A pointer to an Engine instance
 */
void bf2_engine_destroy(Engine** instance);

/**
 * Moves the cell pointer one step forward
 * @author Joshua Behrens
 * @param Engine* A pointer to an initialized Engine instance
 */
void bf2_engine_command_forward(Engine* instance);

/**
 * Moves the cell pointer one step backward
 * @author Joshua Behrens
 * @param Engine* A pointer to an initialized Engine instance
 */
void bf2_engine_command_backward(Engine* instance);

/**
 * Increases the value of the selected cell
 * @author Joshua Behrens
 * @param Engine* A pointer to an initialized Engine instance
 */
void bf2_engine_command_increase(Engine* instance);

/**
 * Decreases the value of the selected cell
 * @author Joshua Behrens
 * @param Engine* A pointer to an initialized Engine instance
 */
void bf2_engine_command_decrease(Engine* instance);

/**
 * Checks whether the selected cell is zero
 * @author Joshua Behrens
 * @param Engine* A pointer to an initialized Engine instance
 * @return int 1, if the selected cell is zero, otherwise 0
 */
int bf2_engine_command_is_cell_zero(Engine* instance);

/**
 * Prints the selected value as ASCII character into the default output
 * @author Joshua Behrens
 * @param Engine* A pointer to an initialized Engine instance
 */
void bf2_engine_command_print(Engine* instance);

#ifdef __cplusplus
}
#endif

#endif // __LIBBRAINFUCK2_ENGINE_H
