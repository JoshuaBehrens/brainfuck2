#ifndef __LIBBRAINFUCK2_ENGINE_HPP
#define __LIBBRAINFUCK2_ENGINE_HPP

#include <vector>
#include <stdint.h>

namespace brainfuck2
{
	/**
	 * A class to handle commands and memory management
	 * @author Joshua Behrens
	 */
	class Engine
	{
	private:
		/**
		 * The structure to hold all the cells to modify
		 * @access private
		 * @author Joshua Behrens
		 */
		std::vector<uint8_t> _memory;

		/**
		 * The current position in the memory block;
		 * @access private
		 * @author Joshua Behrens
		 */
		std::vector<uint8_t>::iterator _position;
	public:
		/**
		 * Creates a new brainfuck2::Engine instance
		 * @access public
		 * @author Joshua Behrens
		 */
		Engine();

		/**
         * Forbids copying brainfuck2::Engine instances
		 * @access public
		 * @author Joshua Behrens
		 * @param const brainfuck2::Engine& copy An instance of brainfuck2::Engine
		 */
		Engine(const Engine& copy) = delete;

		/**
		 * Moves the new brainfuck2::Engine instance
		 * @access public
		 * @author Joshua Behrens
		 * @param brainfuck2::Engine&& move An instance of brainfuck2::Engine
		 */
		Engine(Engine&& move);

		/**
		 * Default destructor
		 * @access public
		 * @author Joshua Behrens
		 */
		~Engine() = default;

		/**
         * Forbids copying brainfuck2::Engine instances
		 * @access public
		 * @author Joshua Behrens
		 * @param const brainfuck2::Engine& copy An instance of brainfuck2::Engine
		 * @return brainfuck2::Engine& A moved brainfuck2::Engine
		 */
		Engine& operator = (const Engine& copy) = delete;

		/**
		 * Moves the new brainfuck2::Engine instance
		 * @access public
		 * @author Joshua Behrens
		 * @param brainfuck2::Engine&& move An instance of brainfuck2::Engine
		 * @return brainfuck2::Engine& A moved brainfuck2::Engine
		 */
		Engine& operator = (Engine&& move);

		/**
		 * Moves the cell pointer one step forward
		 * @access public
		 * @author Joshua Behrens
		 */
		void CommandForward();

		/**
		 * Moves the cell pointer one step backward
		 * @access public
		 * @author Joshua Behrens
		 */
		void CommandBackward();

		/**
		 * Increases the value of the selected cell
		 * @access public
		 * @author Joshua Behrens
		 */
		void CommandIncrease();

		/**
		 * Decreases the value of the selected cell
		 * @access public
		 * @author Joshua Behrens
		 */
		void CommandDecrease();

		/**
		 * Checks whether the selected cell is zero
		 * @access public
		 * @author Joshua Behrens
		 * @return bool true, if the selected cell is zero, otherwise false
		 */
		bool CommandIsCellZero() const;

		/**
		 * Prints the selected value as ASCII character into the default output
		 * @access public
		 * @author Joshua Behrens
		 */
		void CommandPrint() const;
	};
}

#endif // __LIBBRAINFUCK2_ENGINE_HPP
