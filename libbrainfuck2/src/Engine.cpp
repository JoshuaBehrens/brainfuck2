#include <iostream>

#include "Engine.h"

brainfuck2::Engine::Engine()
{
	this->_position = this->_memory.end();
}

brainfuck2::Engine::Engine(brainfuck2::Engine&& move) : Engine()
{
	std::swap(move._memory, this->_memory);
	std::swap(move._position, this->_position);
}

brainfuck2::Engine& brainfuck2::Engine::operator=(brainfuck2::Engine&& move)
{
	std::swap(move._memory, this->_memory);
	std::swap(move._position, this->_position);

	return *this;
}

void brainfuck2::Engine::CommandForward()
{
	if (this->_memory.size() > 0 && this->_position != std::prev(this->_memory.end()))
	{
		std::advance(this->_position, 1);
	}
	else
	{
		this->_memory.push_back(0);
		this->_position = std::prev(this->_memory.end());
	}
}

void brainfuck2::Engine::CommandBackward()
{
	if (this->_position != this->_memory.begin())
	{
		std::advance(this->_position, -1);
	}
}

void brainfuck2::Engine::CommandIncrease()
{
	if (this->_position == this->_memory.end())
	{
		CommandForward();
	}

	++*this->_position;
}

void brainfuck2::Engine::CommandDecrease()
{
	if (this->_position == this->_memory.end())
	{
		CommandForward();
	}

	--*this->_position;
}

bool brainfuck2::Engine::CommandIsCellZero() const
{
	return this->_memory.size() && *this->_position == 0;
}

void brainfuck2::Engine::CommandPrint() const
{
	std::cout.put(CommandIsCellZero() ? 0 : *this->_position);
}

brainfuck2::Engine* bf2_engine_create(void)
{
	return new brainfuck2::Engine();
}

void bf2_engine_destroy(brainfuck2::Engine** instance)
{
	if (instance != nullptr && *instance != nullptr)
	{
		delete *instance;
		*instance = nullptr;
	}
}

void bf2_engine_command_forward(brainfuck2::Engine* instance)
{
	if (instance != nullptr)
	{
		instance->CommandForward();
	}
}

void bf2_engine_command_backward(brainfuck2::Engine* instance)
{
	if (instance != nullptr)
	{
		instance->CommandBackward();
	}
}

void bf2_engine_command_increase(brainfuck2::Engine* instance)
{
	if (instance != nullptr)
	{
		instance->CommandIncrease();
	}
}

void bf2_engine_command_decrease(brainfuck2::Engine* instance)
{
	if (instance != nullptr)
	{
		instance->CommandDecrease();
	}
}

int bf2_engine_command_is_cell_zero(brainfuck2::Engine* instance)
{
	if (instance != nullptr)
	{
		return instance->CommandIsCellZero() ? 1 : 0;
	}

	return 0;
}

void bf2_engine_command_print(brainfuck2::Engine* instance)
{
	if (instance != nullptr)
	{
		instance->CommandPrint();
	}
}
